<?php

namespace Craft;

class MemberAssetsPlugin extends BasePlugin
{
    public function getName()
    {
        return Craft::t('Member Assets');
    }

    public function getVersion()
    {
        return '1.0';
    }

    public function getDeveloper()
    {
        return 'Jeroen Kenters';
    }

    public function getDeveloperUrl()
    {
        return 'http://kenters.com';
    }

    public function hasCpSection()
    {
        return false;
    }

    /**
     * Register control panel routes
     */
    public function registerSiteRoutes()
    {
        return array(
            'memberassets/(?P<id>.\d*)' => array('action' => 'MemberAssets/Assets/download')
        );
    }
}
