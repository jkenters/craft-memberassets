# Craft CMS: MemberAssets plugin #

Quick & dirty plugin to protect member-only assets (require login before you can see the asset on front end).

Works best when moving the Assset Source out of the webroot (set it to ../members for example) so the files are never accessible without this plugin.

Instead of using your Asset URL, link to '/memberassets/{{ asset.id }} The plugin will then lookup the asset by it's ID and show it to the user if he/she is logged in.