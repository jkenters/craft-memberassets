<?php
namespace Craft;

class MemberAssets_AssetsController extends BaseController {

    public function actionDownload() {
        $fileId = $this->actionParams['variables']['matches']['id'];
        $file = craft()->assets->findFile(array('id' => $fileId));
        if($file) {
            header('Content-type: '.$file->getMimeType());
            $path = $_SERVER['DOCUMENT_ROOT'].'/'.$file->getSource()->getAttribute('settings')['path'];
            echo file_get_contents($path.'/'.$file->getAttribute('filename'));
        }
        else {
            echo 'File not found';
        }
        exit;
    }

}